const express = require('express');
const router = express.Router();
const { Users } = require('../controllers');

/* GET home page. */
router.post('/sign-up', Users.create);

router.post('/log-in', Users.login);

module.exports = router;
