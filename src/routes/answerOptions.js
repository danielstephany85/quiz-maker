const express = require('express');
const router = express.Router();
const AnswerOptions = require('../controllers').AnswerOptions;
const {validateToken} = require('../middleware');

router.post('/', validateToken, AnswerOptions.create)

router.get('/', validateToken, AnswerOptions.getByQuestionId)

router.get('/answer-option', validateToken, AnswerOptions.getById)

router.put('/', validateToken, AnswerOptions.update)

router.delete('/', validateToken, AnswerOptions.deleteOne)

module.exports = router;