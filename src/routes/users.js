var express = require('express');
var router = express.Router();
const { Users } = require('../controllers');
const { validateToken } = require('../middleware');

//get a user by id
router.get('/', validateToken, Users.get)

//update a user
router.put('/', validateToken, Users.update)

//delete a user
router.delete('/', validateToken, Users.delete)

module.exports = router;
