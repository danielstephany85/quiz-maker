const express = require('express');
const router = express.Router();
const Questions = require('../controllers').Questions
const { validateToken } = require('../middleware');

//create a new question
router.post('/', validateToken, Questions.create);

//create a quesiton with answerOptions
router.post('/create', validateToken, Questions.createWithOptions)

//get all quesitons for a given quiz
router.get('/', validateToken, Questions.getByQuizId);

//get a question by id
router.get('/question', validateToken, Questions.getById);

//update a question by id
router.put('/', validateToken, Questions.update);

router.delete('/', validateToken, Questions.delete)

module.exports = router;