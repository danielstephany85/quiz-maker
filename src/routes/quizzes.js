const express = require("express");
const router = express.Router();
const Quizzes = require("../controllers").Quizzes;
const { validateToken } = require('../middleware');

//create a new quiz
router.post("/", validateToken, Quizzes.create);

//get all quizzes for a specific user
router.get("/", validateToken, Quizzes.getByUserId);

//get one quiz by id
router.get("/:id", validateToken, Quizzes.getById);

//update a quiz
router.put("/:id", validateToken, Quizzes.update);

//delete a quiz
router.delete("/:id", validateToken, Quizzes.delete);

module.exports = router;