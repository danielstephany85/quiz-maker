const Questions = require("../models").Questions;
const QuestionsService = require('../services/questions');
const answerOptionsService = require('../services/answerOptions');

module.exports = {
    create(req, res){
        const { quizId, type, questionText, instructions, answer, order } = req.body,
            userId = req.user? req.user.id : null;

        if (!quizId || !type || !questionText || !answer || !order || !userId) {
            return res.json({
                status: "error",
                message: "quizId, questionText, type, order, and answer are requried"
            });
        }

        return Questions.create({
            quizId,
            type,
            questionText,
            instructions,
            answer,
            order,
            userId
        })
            .then(question => res.status(201).json({status: "success", data: question}))
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    getById(req, res){
        const id = req.body.id,
            userId = req.user ? req.user.id : null;
        if(!id || !userId){
            return res.json({
                status: "error",
                message: "id and a valid token is requried"
            });
        }

        return Questions.findOne({where: {id, userId}})
            .then(question => res.status(200).json({status: "success", data: question}))
            .catch(error => res.status(400).json({ status: "error", message: error }));

    },

    getByQuizId(req, res) {
        const quizId = req.body.quizId,
            userId = req.user ? req.user.id : null;

        if (!quizId || !userId) {
            return res.json({
                status: "error",
                message: "quizId and a valid userId is requried"
            });
        }

        return Questions.findAll({ where: { quizId, userId }, order: [["order", "ASC"]] })
            .then(questions => res.status(200).json({ status: "success", data: questions }))
            .catch(error => res.status(400).json({ status: "error", message: error }));

    },
    update(req, res) {
        const { id, type, questionText, instructions, answer, order} = req.body,
            userId = req.user ? req.user.id : null;

        if (!id || !type || !questionText || !answer || !order || !userId) {
            return res.json({
                status: "error",
                message: "id, type, questionText, answer, order and a valid token are requried"
            });
        }

        Questions.update({
            type: type,
            questionText: questionText,
            instructions: instructions,
            answer: answer,
            order: order
        }, {
            where: {id, userId},
            returning: true
        })
            .then(result => res.status(201).json({ status: "success", data: result }))
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    delete(req, res) {
        const id = req.body.id,
            userId = req.user ? req.user.id : null

        if (!id || !userId) {
            return res.json({
                status: "error",
                message: "id and a valid token are requried"
            });
        }

        return Questions.destroy({ where: { id, userId } })
            .then(result => {
                if (result === 0) {
                    return res.status(404).json({ status: "error", message: `question with id of ${req.body.id} Not Found` });
                }
                res.status(200).json({ status: "success", data: result })
            })
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    createWithOptions: async function(req, res){
        const userId = req.user ? req.user.id : null
        const options = req.body.answerOptions
        let createdOptions = []
        let questionData;

        try {
            const question = await QuestionsService.create(req.body, userId)

            if(options){
                for(let option of options){
                    const answerOption = await answerOptionsService.create(option, question.id, userId);
                    createdOptions.push(answerOption)
                }
            }
            
            questionData = question.get({ plain: true })
            res.status(201).json({ status: "success", data: { ...questionData, answerOptions: createdOptions} })
        } catch (error) {
            res.status(404).json({ status: "error", message: error })
        }
    }
}
