const Quizzes = require('../models').Quizzes;
const Questions = require('../models').Questions;
const AnswerOptions = require('../models').AnswerOptions;

const baseFieldsToExclude = ['createdAt', 'updatedAt', 'userId']

module.exports = {
    create(req, res) {
        const { title, description} = req.body,
            userId = req.user ? req.user.id : null
        if(!userId || !title){
            return res.json({
                status: "error",
                message: "a title and valid token are requried"
            });
        }
        return Quizzes
            .create({
                title,
                description,
                userId
            })
            .then(quiz => res.status(201).json({ status: "success", data: {quiz} }))
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    getById(req, res) {
        const id = req.params.id,
        userId = req.user ? req.user.id : null;

        if (!id || !userId) {
            return res.json({
                status: "error",
                message: "id and valid token is requried"
            });
        }

        Quizzes.findOne({ 
                where: { id, userId},
                attributes: { exclude: [...baseFieldsToExclude] },
                include: [{
                    model: Questions,
                    as: 'questions',
                    // attributes: ['answer','answerOptions','id','instructions','order','questionText','type'],
                    attributes: { exclude: [...baseFieldsToExclude, 'quizId']},
                    include: [{
                        model: AnswerOptions,
                        as: 'answerOptions',
                        // attributes: ['id','optionText','optionValue','order'],
                        attributes: { exclude: [...baseFieldsToExclude, 'quizId']},
                    }]
                }], 
            })
            .then(quiz => res.status(200).json({ status: "success", data: {quiz} }))
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },
    
    getByUserId(req, res) {
        const userId = req.user ? req.user.id : null;
        if (!userId){
            return res.json({
                status: "error",
                message: "a valid token is requried"
            });
        }

        Quizzes.findAll({where: {userId}})
            .then(quizzes => res.status(200).json({ status: "success", data: {quizzes}}))
        .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    update(req, res) {
        const id = req.params.id,
            {title, description} = req.body,
            userId = req.user ? req.user.id : null;

        if (!id || !userId) {
            return res.json({
                status: "error",
                message: "id and a valid token is requried"
            });
        }

        return Quizzes.update({
            title: title,
            description: description
        }, { where: { id, userId }, returning: true})
            .then(quizzes => {
                res.status(201).json({ status: "success", data: quizzes })
            })
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    delete(req, res){
        const id = req.params.id,
            userId = req.user ? req.user.id : null;

        if (!id || !userId) {
            return res.json({
                status: "error",
                message: "id and a valid token is requried"
            });
        }

        return Quizzes.destroy({where: {id, userId}})
            .then(result => {
                if (result === 0) {
                    return res.status(404).json({ status: "error", message: `quiz with id of ${req.body.id} Not Found` });
                }
                res.status(200).json({ status: "success", data: result })
            })
            .catch(error => res.status(400).json({ status: "error", message: error }));
    }

}