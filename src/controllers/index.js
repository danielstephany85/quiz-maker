const Users = require("./users");
const Quizzes = require("./quizzes");
const Questions = require("./questions");
const AnswerOptions = require("./answerOptions");

module.exports = {
    Users: Users,
    Quizzes: Quizzes,
    Questions: Questions,
    AnswerOptions: AnswerOptions
}