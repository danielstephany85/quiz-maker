const AnswerOptions = require("../models").AnswerOptions
const AnswerOptionsServices = require("../services/answerOptions");


const create = async function(req, res) {        
    try { 
        const userId = req.user ? req.user.id : null;   
        const answerOption =  await AnswerOptionsServices.create(req.body, userId);

        res.status(201).json({ status: "success", data: answerOption })
    } catch (error) {
        res.status(404).json({ status: "error", message: error })
    }
}

const getById = (req, res) => {
    const id = req.body.id,
        userId = req.user ? req.user.id : null;

    if (!id || !userId) {
        return res.status(400).json({
            status: "error",
            message: "id is required"
        });
    }
    return AnswerOptions.findOne({ where: { id, userId } })
        .then(answerOption => res.status(201).json({ status: "success", data: answerOption }))
        .catch(error => res.status(404).json({ status: "error", message: error }));
}

const getByQuestionId = (req, res) => {
    const questionId = req.body.questionId,
        userId = req.user ? req.user.id : null;

    if (!questionId || !userId) {
        return res.status(400).json({
            status: "error",
            message: "questionId is required"
        });
    }

    return AnswerOptions.findAll({ where: { questionId, userId }, order: [['order', 'ASC']] })
        .then(answerOption => res.status(201).json({ status: "success", data: answerOption }))
        .catch(error => res.status(404).json({ status: "error", message: error }));
}

const update = (req, res) => {
    const {id, optionText, optionValue, order} = req.body,
        userId = req.user ? req.user.id : null;

    if (!id || !optionText || !optionValue || !order || !userId) {
        return res.status(400).json({
            status: "error",
            message: "id, optionText, optionValue, and a valid token are required"
        })
    }

    return AnswerOptions.update({
        optionText,
        optionValue,
        order
    },{where: {id, userId}, returning: true})
        .then(answerOption => res.status(201).json({ status: "success", data: answerOption }))
        .catch(error => res.status(404).json({ status: "error", message: error }));
}

const deleteOne = (req, res) => {
    const id = req.body.id,
        userId = req.user ? req.user.id : null;

    if (!id || !userId) {
        return res.status(400).json({
            status: "error",
            message: "id and a vaild token are required"
        });
    }

    return AnswerOptions.destroy({where: {id, userId}})
        .then(result => {
            if (result === 0) {
                return res.status(404).json({ status: "error", message: `answerOption with id of ${id} Not Found` });
            }
            res.status(200).json({ status: "success", data: result })
        })
        .catch(error => res.status(400).json({ status: "error", message: error }));
}

module.exports = {
    create,
    getById,
    getByQuestionId,
    update,
    deleteOne
}
