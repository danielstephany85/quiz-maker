const Users = require('../models').Users;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config.js');

module.exports = {
    create(req, res, next){
        const { firstName, lastName, email, password} = req.body;
        
        if(!firstName || !lastName || !email || !password){
            return res.status(400).json({
                status: "error",
                message: "firstName, lastName, email, and password are required"
            });
        }

        return Users.findOne({where: {email: email}})
            .then(existingUser => {
                if (existingUser) {
                    return res.status(400).json({
                        status: "error",
                        message: `user with email ${email} already exists`
                    });
                } 

                bcrypt.genSalt(10, function (err, salt) {
                    if (err) return next(err)
                    bcrypt.hash(password, salt, function (err, hash) {
                        if (err) return next(err)

                        Users
                           .create({
                               firstName,
                               lastName,
                               email,
                               password: hash
                           })
                           .then(user => {
                               const token = jwt.sign({ id: user.id }, config.jwtSecret);
                               return res.status(201).json({status: "success", data: {user, token}});
                            })
                           .catch(error => res.status(400).json({status: "error", message: error}));
                    });
                });
            })
            .catch(error => res.status(400).json({ status: "error", message: error }));

    },

    login(req, res, next){
        const {email, password} = req.body;

        if(!email || !password){
            return res.status(400).json({
                status: "error",
                message: "email and password are required"
            });
        }

        return Users.findOne({where: {email: email}})
            .then(user => {
                bcrypt.compare(password, user.password)
                    .then(isMatch => {
                        if(isMatch){
                            const token = jwt.sign({ id: user.id }, config.jwtSecret);
                            return res.status(201).json({ status: "success", data: { user, token } });
                        }
                        return res.status(400).json({ status: "error", message: "wrong password" });
                    })
                    .catch(e => next(e))
            })
            .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    get(req, res){
        if(!req.user) {
            return res.status(400).json({
                status: "error",
                message: 'token is requried',
            });
        }

        return Users.findOne({ where: { id: req.user.id }, attributes: { exclude: ["password"] }})
        .then(user => {
            if (!user) {
                return res.status(404).json({
                    status: "error",
                    message: 'user Not Found',
                });
            }
            res.status(200).json({ status: "success", data: {user} })
        })
        .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    update(req, res) {
        const { firstName, lastName, email, password} = req.body;
        if (!req.user) {
            return res.status(400).json({
                status: "error",
                message: 'token is requried',
            });
        }

        return Users.findOne({where: {id: req.user.id}})
        .then(user => {

            function update(password){
                Users.update({
                    firstName: firstName || user.firstName,
                    lastName: lastName || user.lastName,
                    email: email || user.email,
                    password: password || user.password
                },
                    { where: { id: req.user.id }, returning: true })
                    .then(result => {
                        if (!result[0]) {
                            return res.status(404).json({
                                status: "error",
                                message: 'user Not Found',
                            });
                        }
                        res.status(200).json({ status: "success", data: result })
                    })
                    .catch(error => res.status(400).json({ status: "error", message: error }));
            }
            
            if (password) {
                bcrypt.genSalt(10, function (err, salt) {
                    if (err) return next(err)
                    bcrypt.hash(password, salt, function (err, hash) {
                        if (err) return next(err)
                        update(hash);
                    });
                });
            } else {
                update()
            }
            
        })
        .catch(error => res.status(400).json({ status: "error", message: error }));
    },

    delete(req, res) {
        if (!req.user) {
            return res.status(400).json({
                status: "error",
                message: 'token is requried',
            });
        }

        return Users.destroy({where: {id: req.user.id}})
        .then(result => {
            if(result === 0){
                return res.status(404).json({status: "error", message: 'user Not Found'});
            }
            res.status(200).json({status: "success", data: result})
        })
        .catch(error => res.status(400).json({status: "error", message: error}));
    }
}