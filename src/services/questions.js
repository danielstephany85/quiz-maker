const Questions = require("../models").Questions;

const create = (data, userId) => new Promise((resolve, reject) => {
    const { quizId, type, questionText, instructions, answer, order } = data

    if (!quizId || !type || !questionText || !answer || !order || !userId) {
        return reject("quizId, questionText, type, order, and answer are requried")
    }

    Questions.create({
        quizId,
        type,
        questionText,
        instructions,
        answer,
        order,
        userId
    })
    .then(question => resolve(question))
    .catch(error => reject(error));
})

module.exports = {
    create
}