const AnswerOptions = require("../models").AnswerOptions;

module.exports = {
    create: (reqBody, questionId, userId) => new Promise((resolve, reject) => {
        const { optionText, optionValue, order } = reqBody

        if (!optionText || !optionValue || typeof questionId === 'undefined' || typeof order === 'undefined' || typeof userId === 'undefined') {
            return reject("optionText, optionValue, questionId, order and a valid token are required")
        }

        AnswerOptions.create({
            optionText,
            optionValue,
            questionId,
            order,
            userId
        })
            .then(answerOption => resolve(answerOption))
            .catch(error => reject(error));
    })
}