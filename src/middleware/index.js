const jwt = require('jsonwebtoken');
const config = require('../config.js');
const {Users} = require('../models');

module.exports = {
    validateToken(req, res, next){
        const token = req.body.token || req.query.token || req.headers['x-access-token'];

        if(token){
            return jwt.verify(token, config.jwtSecret, function (err, data) {
                Users.findOne({where: {id: data.id}})
                    .then(user => {
                        if(user){
                            req.user = user;
                            return next()
                        }
                        return res.status(404).json({ status: "error", message: "user not found" })
                    })
                    .catch(error => res.status(404).json({ status: "error", message: error }));
            });
        }

        return res.status(400).json({
            status: "error",
            message: "token is required"
        });
    }
}