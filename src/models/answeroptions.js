'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AnswerOptions extends Model {
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });
      this.belongsTo(models.Questions, {
        foreignKey: 'questionId',
        onDelete: 'CASCADE',
      });
    }
  };
  AnswerOptions.init({
    optionText: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    optionValue: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'AnswerOptions',
  });
  return AnswerOptions;
};