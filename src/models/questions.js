'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Questions extends Model {
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });
      
      this.belongsTo(models.Quizzes, {
        foreignKey: 'quizId',
        onDelete: 'CASCADE',
      });

      this.hasMany(models.AnswerOptions, {
        foreignKey: 'questionId',
        as: "answerOptions"
      });
    }
  };
  Questions.init({
    questionText: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    instructions: DataTypes.STRING,
    answer: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'Questions',
  });
  return Questions;
};