'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Quizzes extends Model {
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });

      this.hasMany(models.Questions, {
        foreignKey: 'quizId',
        as: 'questions'
      });
    }
  };
  Quizzes.init({
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Quizzes',
  });
  return Quizzes;
};