'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      // define association here
      this.hasMany(models.Quizzes, {
        foreignKey: 'userId',
        as: 'quizzes',
      });
      this.hasMany(models.Questions, {
        foreignKey: 'userId',
        as: 'questions',
      });
      this.hasMany(models.AnswerOptions, {
        foreignKey: 'userId',
        as: 'answerOptions',
      });
    }

    toJSON(){
      const values = Object.assign({}, this.get());

      delete values.password;
      return values;
    }
  };
  Users.init({
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};